# Dev Machine Requirements
`npm install --global expo-cli`
`brew bundle --file=./setup/Brewfile`

- Install android studio: https://developer.android.com/studio
- Install Xcode: https://developer.apple.com/xcode

### Git
- User
  - `git config [--global] user.name "Rudy"`
  - `git config [--global] user.email "rudreshsj@gmail.com"`
- `.gitmessage`
  - `git config commit.template .gitmessage`
- Signed commits
  - [https://docs.github.com/en/github/authenticating-to-github/generating-a-new-gpg-key](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-gpg-key)
  - [https://gist.github.com/troyfontaine/18c9146295168ee9ca2b30c00bd1b41e](https://gist.github.com/troyfontaine/18c9146295168ee9ca2b30c00bd1b41e)

# Run the project
`npm start`
